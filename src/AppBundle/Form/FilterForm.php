<?php
/**
 * Created by PhpStorm.
 * User: Parinaz
 * Date: 12/05/2016
 * Time: 15:48
 */

namespace AppBundle\Form;


use AppBundle\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FilterForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('note', ChoiceType::class, array(
                'choices'   => Comment::getNotes(),
                'label'     => 'Note',
            ))
        ;
    }


    public function getName()
    {
        return 'comment_filterform';
    }
}