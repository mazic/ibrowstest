<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Comment;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadComments implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    public function load(ObjectManager $manager)
    {
        $connection = $manager->getConnection();
        $connection->exec("ALTER TABLE app_user AUTO_INCREMENT = 1;");
        $connection->exec("ALTER TABLE fos_user AUTO_INCREMENT = 1;");

        $entry = new Comment();
        $entry->setEmail('default@test.com');
        $entry->setSubject("test 1");
        $entry->setBody("Everything was Ok");
        $entry->setNote(5);
        $manager->persist($entry);
        
        $manager->flush();
    }
}