<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Comment;
use AppBundle\Form\CommentType;
use AppBundle\Form\FilterForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class MainController
 * @package AppBundle\Controller
 *
 */
class MainController extends Controller
{
    /**
     * Dashboard of Guestbook test
     * Lists all Comment entities with filter.
     *
     * @param Request $request
     * @return Response
     * @Route("/", name="overview")
     *
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(new FilterForm());
        $data = $request->get($form->getName());

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Comment')->fetchComments($this->getCriteria($data));

        $entities = $this->getPaginator($request, $query);
        return $this->render('AppBundle:Main:index.html.twig', array(
                'entities' => $entities,
                'form' => $form->createView(),
            ));

    }

    /**
     * Creates a new comment entity.
     *
     * @param Request $request
     * @return Response
     * @Route("/new", name="comment_create")
     * @Method("POST")
     *
     * @Security("has_role('ROLE_USER')")
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Comment();
        $form = $this->createForm(new CommentType(), $entity);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $entity->setEmail($this->getUser()->getEmail());

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->set('success', 'Your comment has bees sent ! Thank you.');

            return $this->redirect($this->generateUrl('overview'));
        }

        return $this->render('AppBundle:Main:new.html.twig', array(
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new comment entity.
     *
     *
     * @return Response
     * @Route("/new", name="comment_new")
     * @Method("GET")
     *
     * @Security("has_role('ROLE_USER')")
     *
     */
    public function newAction()
    {
        if (!$this->isNewComment()) {
            $this->get('session')->getFlashBag()->set('success', 'Your comment has bees already sent ! Thank you.');

            return $this->redirect($this->generateUrl('overview'));
        }

        $entity  = new Comment();
        $entity->setEmail($this->getUser()->getEmail());
        $form = $this->createForm(new CommentType(), $entity);

        return $this->render('AppBundle:Main:new.html.twig', array(
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Comment entity. with ParamConverter .
     *
     * @param Comment $comment
     * @return Response
     * @Route("/show/{id}", name="comment_show")
     * @Method("GET")
     *
     */
    public function showAction(Comment $comment)
    {
        //using of param converter
        return $this->render('AppBundle:Main:show.html.twig', array(
            'entity'      => $comment,
        ));
    }

    /**
     * to be passed to fetchComments method of Comment Repository
     * @param $data
     * @return array
     */
    protected function getCriteria($data)
    {
        $criteria = array();
        if (isset($data['note']) && $data['note']) {
            $criteria['note'] = $data['note'];
        }

        return $criteria;
    }

    /**
     * TODO: define it as service to be used globally
     * @param Request $request
     * @param $query
     * @return mixed
     */
    protected function getPaginator(Request $request, $query)
    {
        $paginator  = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );
        return $entities;
    }

    /**
     * True if there in no comment with the current user's email
     * @return bool
     */
    protected function isNewComment()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('AppBundle:Comment')->findOneByEmail($user->getEmail());
        if (!$comment) {
            return true;
        }

        return false;
    }
}
