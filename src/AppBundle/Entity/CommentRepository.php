<?php


namespace AppBundle\Entity;


use Doctrine\ORM\EntityRepository;


class CommentRepository extends EntityRepository
{


    public function fetchComments($criteria = array())
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a')
            ->from('AppBundle:Comment', 'a')
            ->orderBy('a.createdAt', 'DESC')
        ;

        if (isset($criteria['note']) && !empty($criteria['note'])) {

            $qb->andWhere('a.note = :note');
            $qb->setParameter('note', $criteria['note']);
        }

        return $qb;
    }



    /*
     * to fetch user with comment
     *
     */
    public function search(array $criteria = null, array $orderBy = null, $limit = null, $offset = null)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('a, u')->from($this->getClassName(), 'a')->innerJoin('a.user', 'u');

        if (is_array($orderBy) && !empty($orderBy)) {
            foreach ($orderBy as $key => $value) {
                if (preg_match('^user\..*', $key)) {
                    $key = preg_replace('^user\..*', 'u.', $key);
                } elseif (!preg_match('/^a\./', $key)) {
                    $key = "a." . $key;
                }

                if (!in_array(strtolower($value), ['asc', 'desc'])) {
                    $value = 'asc';
                }

                $qb->addOrderBy($key, $value);
            }
        }

        if (is_array($criteria) && !empty($criteria)) {
            foreach ($criteria as $index => $criterion) {
                if (is_array($criterion) && !empty($criterion)) {
                    list($key, $op, $value) = $criterion;
                    if (preg_match("/^user\./i", $key)) {
                        $key = preg_replace("/^user\./i", 'u.', $key);
                    } elseif (!preg_match('/^a\./', $key)) {
                        $key = "a." . $key;
                    }


                    if ($op) {
                        $param = ':pHolder' . count($qb->getParameters());
                        $qb->andWhere(trim($key) . ' ' . trim($op) . ' ' . trim($param));
                        $qb->setParameter($param, $value);
                    } else {
                        $qb->andWhere(trim($key) . ' ' . trim($value));
                    }
                }
            }
        }

        return $qb;
    }

}