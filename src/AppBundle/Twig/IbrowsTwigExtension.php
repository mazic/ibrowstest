<?php
/**
 * Created by PhpStorm.
 * User: Parinaz  ===> it's me Maxime ;)
 * Date: 12/05/2016
 * Time: 16:55
 */


namespace AppBundle\Twig;

use AppBundle\Entity\Comment;

class IbrowsTwigExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('notation', array($this, 'notationFilter')),
            new \Twig_SimpleFilter('sexlabel', array($this, 'sexFilter')),
        );
    }

    public function notationFilter($note)
    {
        if (in_array($note, Comment::getNotes())) {
            $notes = Comment::getNotes();

            return $notes[$note];
        }

        return $note;
    }


    public function getName()
    {
        return 'ibrows_extension';
    }
}
