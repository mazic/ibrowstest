Guestbook Challenge
==========

A Symfony project created on May 12, 2016, 14:35 pm.

Functionality to be implemented:
- Overview of all entries with search and filter possibility
- View all detailed about an entry
- Create a new entry


Bundles activated :
- FOSUserBundle
- DoctrineFixture (dev only)
- KnpPaginatorBundle


